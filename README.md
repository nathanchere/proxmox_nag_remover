# proxmox nag remover

Just as it says. Removes the annoying "you don't have a subscription" nags from [Proxmox Virtual Environment](https://www.proxmox.com/en/proxmox-virtual-environment/overview).

## Why?

There are some similar scripts already on Github but the most popular ones I've seen are quick hacks which were quickly abandoned and then no longer working properly with later PVE releases.

My approach is a bit more aggressive than what I've seen elsewhere. Wheter or not it holds up with future releases remains to be seen but I'll try to keep it updated in the event of any breaking changes.

## Usage

Run the script from a shell on your PVE instance. There are better ways to do this but if you have no idea how and need a step-by-step guide:

* copy the contents of `clean.sh` to your clipboard
* launch a shell from the Proxmox web GUI
* run `vi clean.sh`
* type 'i' for insert mode
* right-click and "Paste as plain text" (or if that's not available, just "Paste") to dump the script contents
* type `:wq` and press Enter to save changes and quit

Run:

```sh
sh clean.sh
```

Test:

```sh
sh clean.sh -t
```

Test locally without deploying the script to Proxmox first (mostly for my development convenience):

```sh
sh clean.sh -d "./reference/8.0.3" -t
```

## How it works

Pretty much all the UI code exists in `proxmoxlib.js`. We find the "checked_command" function there and remove all the unnecessary subscription check logic
so it just executes `orig_cmd` whenever called.

The script is well commented, please review what it does before you run it. No liability is accepted if it breaks your installation, leaks nuclear launch codes etc.

Backups are written automatically whenever the script is run. If already present, you will be prompted to resotre the backup before running again to ensure you're not patching over
an already patched file. It shouldn't matter but might as well be on the safe side. HOWEVER - if you have recently upgraded your system, do NOT restore the backup as it will probably
be a backup of the previous version and may give unexpected results. 

## Caveats

If you ruin `proxmoxlib.js` you essentially lose all web UI functionality. If that happens remote in via SSH and restore `proxmoxlib.js.bak` to get up and running again:

```
cp /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js.bak /usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js
systemctl restart pveproxy.service
```

Whenever you install updates there's a good chance it will overwrite the changes to `proxmoxlib.js`. Just remote in and run `clean.sh` again. As long as you deployed it to the default $HOME folder it should survive between updates and not need to be deployed from scratch again.

## TODO

Still need to choose an appropriate license

Until then, this is not licensed at all.
