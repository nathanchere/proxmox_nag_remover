#!/bin/sh

##################################################
##################################################
##
## Get the subscription nags out of proxmoxlib.js
##
## https://gitlab.com/nathanchere/proxmox_nag_remover
##
##################################################
##################################################

IFS="$(printf '\n\t')"
set -e

error_exit() {
    echo "[ERROR] $1" >&2
    exit 1s
}

##################################################
# Config
##################################################

WORKING_DIR="/usr/share/javascript/proxmox-widget-toolkit"
FILE_NAME="proxmoxlib.js"

MIN_VERSION="8.0.0"
MAX_VERSION="8.1.3"

PREVIEW_ONLY=false

while getopts ":h?d:f:tp" opt; do
    case $opt in
        d)
            WORKING_DIR=$OPTARG
            FILE_PATH="${WORKING_DIR}/${FILE_NAME}"
            ;;
        f)
            FILE_NAME=$OPTARG
            FILE_PATH="${WORKING_DIR}/${FILE_NAME}"
            ;;
        t|p)
            PREVIEW_ONLY=true
            ;;
        h|\?)
            echo "Usage: $0 [options]"
            echo "Options:"
            echo "  -f       Override the file name to patch."
            echo "  -d       Overide the directory where to look for the file"
            echo "  -p, -t   Preview/test mode"
            echo "             See what would be replaced without writing any changes"
            echo "  -h, -?   Display this help message"
            display_help
            exit 0
        ;;*)
            echo "Invalid option: -$OPTARG" >&2
            exit 1
            ;;
    esac
done

FILE_PATH="${WORKING_DIR}/${FILE_NAME}"

display_config() {
    echo "FILE_PATH: ${FILE_PATH}"
    echo "WORKING_DIR: ${WORKING_DIR}"
    echo "FILE_NAME: ${FILE_NAME}"
    if [ "$PREVIEW_ONLY" = true ]; then
        echo "MODE: preview"
    else
        echo "MODE: live"
    fi
}

##################################################
# Version checking
##################################################

extract_version() {
    echo "$1" | cut -d '/' -f 2
}

version_lte() {
    [ "$1" = "$(printf "%s\n%s" "$1" "$2" | sort -V | head -n1)" ]
}

is_supported_version() {
    local VERSION="$1"
    version_lte "$MIN_VERSION" "$VERSION" && version_lte "$VERSION" "$MAX_VERSION"
}

check_version() {
    echo "Checking compatiblity..."

    OS_VERSION_FULL=$(pveversion) || error_exit "Failed to get Proxmox version."
    OS_VERSION=$(extract_version "$OS_VERSION_FULL")

    if is_supported_version "$OS_VERSION"; then
        echo "Detected PVE version v$OS_VERSION"
    else
        echo "[WARNING] Detected PVE v$OS_VERSION" 
        echo "This script is intended for PVE v$MIN_VERSION to v$MAX_VERSION"
        echo "Do you wish to continue anyway? (y/N)"
        read -r yn
        case $yn in
            [Yy]* ) echo "Continuing with increased risk of dragons...";;
            * ) error_exit "Script aborted by the user.";;
        esac
    fi
}

##################################################
# Backups
##################################################

backup() {
    #  Make a backup of the js file before we mess with it.
    #  If a backup already exists, restore that first to help prevent
    #  modifying an already-modifed file.
    #  If no backup exists, it will create one.

    BACKUP_FILE="${FILE_PATH}.bak"
    echo "Creating backup..."
    if [ -f "$BACKUP_FILE" ]; then
        echo "[WARNING] $BACKUP_FILE already exists."
        echo "Do you wish to restore the backup patching to revert any existing changes?"
        echo "If you have recently upgraded your system, do not restore the backup as it will"
        echo "likely be of the previous version."
        echo "Do you restore backup? (y/N)"
        read -r confirm_restore
        case $confirm_restore in
            [Yy]* )
                echo "Restoring backup before continuing..."
                cp "$BACKUP_FILE" "$FILE_PATH" || error_exit "Failed to restore the backup."
                ;;
            [Nn]* )
                echo "Continuing without restoring backup..."
                ;;
            * )
                error_exit "Restore aborted."
                ;;
        esac
    else
        cp "$FILE_PATH" "$BACKUP_FILE" || error_exit "Failed to create a backup."
        echo "Created $BACKUP_FILE"
    fi
}

##################################################
# Patch
##################################################

patch() {
    START_PATTERN="$1"
    END_PATTERN="$2"
    REPLACEMENT="$3"
    DESCRIPTION="$4"

    echo -e "\n--==[ ${DESCRIPTION} ]==--"

    if perl -0777 -ne "exit 1 unless /\Q$START_PATTERN\E.*?\Q$END_PATTERN\E/s" "$FILE_PATH"; then
        if [ "$PREVIEW_ONLY" = "false" ]; then
            echo "Match found; patching..."
            perl -i -0777 -pe 's/\Q'"$START_PATTERN"'\E.*?\Q'"$END_PATTERN"'\E/'"$START_PATTERN$REPLACEMENT$END_PATTERN"'/sg' "$FILE_PATH"
        else
            echo "Match found:"
            perl -0777 -ne "print '$START_PATTERN'.\$1 while /\Q$START_PATTERN\E(.*?)\Q$END_PATTERN\E/sg" "$FILE_PATH"
            echo "Replacing to:" 
            perl -0777 -pe 's/.*\Q'"$START_PATTERN"'\E.*?\Q'"$END_PATTERN"'\E.*/'"$START_PATTERN$REPLACEMENT"'/sg' "$FILE_PATH"
       fi
    else
        echo "No match found."
    fi
}

##################################################
# main
##################################################

display_config

if [ "$PREVIEW_ONLY" = false ]; then
    check_version
    backup
fi

patch 'checked_command: function(orig_cmd) ' \
      'assemble_field_data: function' \
      '{ orig_cmd(); },' \
      'checked_command() wrapper'


##################################################
# cleanup
##################################################

if [ "$PREVIEW_ONLY" = false ]; then
    echo "The pveproxy service should be restarted now. If you are connected via remote session, this may disconnect you."
    echo "It's OK. You can reconnect. You may also need to clear your browser cache to see the changes to the proxmox portal."
    echo "Do you wish to restart the service? (y/N)"
    read -r yn
    case $yn in
        [Yy]* ) echo "Restarting; see you on the other side..." 
        # systemctl restart pveproxy.service
        ;;
        * ) echo "Script complete. You can manually restart the service with:"
            echo "  systemctl restart pveproxy.service"
            ;;
    esac
fi


